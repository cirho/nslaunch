#define _GNU_SOURCE
#include <dirent.h>
#include <errno.h>
#include <fcntl.h>
#include <linux/limits.h>
#include <stdio.h>
#include <sched.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>
#include <sys/mount.h>
#include <sys/stat.h>
#include <unistd.h>

#define throw_errno(fmt, ...) \
    do { fprintf(stderr, fmt ": %s \n", ##__VA_ARGS__, strerror(errno)); return -EXIT_FAILURE; } while(0)
#define throw_log(fmt, ...) \
    do { fprintf(stderr, fmt "\n", ##__VA_ARGS__); return -EXIT_FAILURE; } while(0)

static bool uses_init_netns(void) {
    struct stat self_stat, init_stat;

    if (stat("/proc/self/ns/net", &self_stat))
        throw_errno("could non stat current nsnet");

    if (stat("/proc/1/ns/net", &init_stat))
        throw_errno("could non stat current nsnet");

    return self_stat.st_ino == init_stat.st_ino && self_stat.st_dev == init_stat.st_dev;
}

static int switch_netns(const char* path) {
    int fd;

    if ((fd = open(path, O_RDONLY | O_CLOEXEC)) < 0)
        throw_errno("could not open netns file %s", path);

    if (setns(fd, CLONE_NEWNET)) {
        close(fd);
        throw_errno("setns failed");
    }

    close(fd);

    return 0;
}

static int enslave_mntns(const char* name) {
    if (unshare(CLONE_NEWNS))
        throw_errno("could not unshare mntns");

    // Prevent mounts from propagating back.
    if (mount("", "/", "none", MS_SLAVE | MS_REC, nullptr))
        throw_errno("could not recursively mount / as slave");

    if (umount2("/sys", MNT_DETACH))
        throw_errno("could not unmount /sys");

    if (mount(name,"/sys", "sysfs", 0, nullptr))
        throw_errno("could not remount /sys");

    return 0;
}

static int bind_etc(const char *path) {
    char etc_path[PATH_MAX];
    char src_path[PATH_MAX];
    struct dirent *entry;
    DIR *dir;

    if (!(dir = opendir(path)))
        return errno == ENOENT ? 0 : -1;

    while ((entry = readdir(dir)) != nullptr) {
        if (strlen(entry->d_name) > PATH_MAX / 2)
            throw_log("one of files has a too long name");
		if (strcmp(entry->d_name, ".") == 0)
			continue;
		if (strcmp(entry->d_name, "..") == 0)
			continue;

		snprintf(src_path, sizeof(src_path), "%s/%s", path, entry->d_name);
		snprintf(etc_path, sizeof(etc_path), "/etc/%s", entry->d_name);

		if (mount(src_path, etc_path, "none", MS_BIND, nullptr)) {
            closedir(dir);
            throw_errno("could not bind a file (%s -> %s)", src_path, etc_path);
        }
	}

	closedir(dir);

    return 0;
}

int program(int argc, char *argv[]) {
    uid_t ruid, euid, suid;
    gid_t rgid, egid, sgid;

    if (argc < 2)
        throw_log("no command specified");

    if (getresuid(&ruid, &euid, &suid))
        throw_errno("could not fetch uids");
    if (getresgid(&rgid, &egid, &sgid))
        throw_errno("could not fetch gids");

    if (suid != 0 || euid != 0)
        throw_log("lack of suid flag");

    if (rgid != egid || rgid != sgid)
        throw_log("excessive sgid flag");

    if (!uses_init_netns())
        throw_log("nslaunch supports only launching from pid 1 namespace");

    if (switch_netns("/run/netns/physical"))
        throw_log("could not switch namespace");

    if (enslave_mntns("physical"))
        throw_log("could not open corresponding mntns");

    if (bind_etc("/etc/netns/physical"))
        throw_log("could not bind etc files");

    // Close all file descriptors above stderr to minimize risk of passing descriptors between namespaces.
    closefrom(STDERR_FILENO + 1);

    if (setresuid(ruid, ruid, ruid))
        throw_errno("could not restore uids");
    if (setresgid(rgid, rgid, rgid))
        throw_errno("could not restore gids");

    // There is no need to sanitize env or PATH, because all dangerous operations
    // are executed in the normal user context. We do not care about malicious programs getting access to physical
    // namespace considering they can bad things even in original one.
    if (execvp(argv[1], argv + 1))
        throw_errno("could not execute command");

    return 0;
}

int main(int argc, char *argv[]) {
    return -program(argc, argv);
}
